 import java.util.ArrayList;

public class VariateurLumiere implements Appareil{
	
	private Lumiere lum;
		
	public VariateurLumiere(){
		this.lum = new Lumiere();
	}
	
	public VariateurLumiere(Lumiere l){
		this.lum = l;
	}
	
	public void allumer(){
		this.lum.changerIntensite(10);
	}
	
	public void eteindre(){
		this.lum.changerIntensite(0);
		
	}
	
	public boolean isAllume(){
		boolean res = false;
		if (lum.getLumiere() != 0) {
			res = true;
		}
		return res;
			
	}
	
	public String toString() {
		String r = "";
		r += "VarLumiere : " + lum.toString();
		return (r);
	}
}


