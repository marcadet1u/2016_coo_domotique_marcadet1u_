import static org.junit.Assert.*;

import org.junit.Test;

/**
 * classe de test permettant de tester la classe Telecommande
 */
public class TelecommandeTest {

	@Test
	/**
	 * test de l'ajout d'une lampe lorsque la telecommande est vide
	 */
	public void testTelecommande_ajoutLampe_TelecommandeVide() {
		Telecommande t = new Telecommande();
		Lampe l = new Lampe("lampe1");
		t.ajouterAppareil(l);
		assertEquals("une nouvelle lampe devrait etre ajouter", t.getAppareil(1), l  );
	}
	
	
	/**
	 * test de l'ajout d'une lampe lorsque la telecommande comporte plus d'une lampe
	 */
	@Test
	public void testTelecommande_ajoutLampe_Telecommande1Element() {
		Telecommande t = new Telecommande();
		Lampe l1 = new Lampe("lampe1");
		Lampe l2 = new Lampe("lampe2");
		t.ajouterAppareil(l1);
		t.ajouterAppareil(l2);
		assertEquals("une nouvelle lampe devrait etre ajoutee", t.getAppareil(1), l1  );
		assertEquals("une nouvelle lampe devrait etre ajoutee", t.getAppareil(2), l2  );
	}
	
	
	/**
	 * test de l'activation d'une lampe lorsque celle-ci est eteinte
	 */
	@Test
	public void testTelecommande_activationLampe_Position_0() {
		Telecommande t = new Telecommande();
		Lampe l = new Lampe("lampe1");
		t.ajouterAppareil(l);
		t.activerAppareil(0);
		assertEquals("une nouvelle lampe devrait etre ajouter",t.getAppareil(1).isAllume(),  false);
	}
	
	/**
	 * test de l'activation d'une lampe lorsque celle-ci est allumee
	 */
	@Test
	public void testTelecommande_activationLampe_Position_1() {
		Telecommande t = new Telecommande();
		Lampe l = new Lampe("lampe1");
		t.ajouterAppareil(l);
		t.activerAppareil(0);
 	}
	
	/**
	 * test de l'activation d'une lampe lorsque celle-ci n'existe pas
	 */
	@Test
	public void testTelecommande_activationLampe_LampeInexistante() {
		Telecommande t = new Telecommande();
		Lampe l = new Lampe("lampe1");
		t.ajouterAppareil(l);
		t.activerAppareil(0);
		assertEquals("une nouvelle lampe devrait etre ajoutee", t.getAppareil(1).isAllume(), false);
	}
	
	@Test
	public void testTelecommande_activationLumiere() {
		Telecommande t = new Telecommande();
		Lumiere l = new Lumiere();
		VariateurLumiere v = new VariateurLumiere(l);
		t.ajouterAppareil(v);
		t.activerAppareil(0);
	}
	
	@Test
	public void testTelecommande_desactivationLumiere() {
		Telecommande t = new Telecommande();
		Lumiere l = new Lumiere();
		VariateurLumiere v = new VariateurLumiere(l);
		t.ajouterAppareil(v);
		t.desactiverAppareil(0);
	}
	
	
}