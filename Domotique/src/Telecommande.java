 import java.util.ArrayList;


public class Telecommande {
	
	private ArrayList<Appareil> appareils;
	
	public Telecommande(){
		appareils = new ArrayList<Appareil>();
	}
	
	public Appareil getAppareil(int indiceAppareil) {
		if ((indiceAppareil >= 0) && (indiceAppareil < this.appareils.size())) {
			return this.appareils.get(indiceAppareil);
		}
		else {
			return null;
		}
	}
	
	public void ajouterAppareil( Appareil l){
		this.appareils.add(l);
	}
	
	public void activerAppareil(int indiceAppareil){
		this.appareils.get(indiceAppareil).allumer();
	}
	
	public void desactiverAppareil( int indiceAppareil){
		this.appareils.get(indiceAppareil).eteindre();
	}
	
	public void activerTout(){
		for(int i = 0 ; i < this.appareils.size() ; i++) {
			this.appareils.get(i).allumer();
		}
	}
	
	public int getNombre(){
		return this.appareils.size();
	}
	
	public String toString(){
		
		
		
		
		return " La telecommande controle : " + this.appareils + ".";
	}

	
}